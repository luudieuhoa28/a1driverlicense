package com.example.driverlicense.entity;

import java.io.Serializable;

public class Question implements Serializable {

    private int questionId;
    private String questionContent;
    private String questionImg;
    private String option1;
    private String option2;
    private String option3;
    private String option4;
    private String answerDescription;
    private int answer;
    private int questionType;
    private boolean isLearned;
    private int mark;
    private boolean isWrong;
    private boolean isIncludeA1;
    private boolean isIncludeA2;
    private boolean isIncludeA34;
    private boolean isIncludeB1;
    private boolean isIncludeB2;
    private boolean isIncludeC;
    private boolean isIncludeDEF;
    private boolean questionDie;
    private String awsa1;
    private int ent;

    public Question() {
    }

    public Question(int questionId, String questionContent, String questionImg, String option1, String option2, String option3, String option4, String answerDescription, int answer, int questionType, boolean isLearned, int mark, boolean isWrong, boolean isIncludeA1, boolean isIncludeA2, boolean isIncludeA34, boolean isIncludeB1, boolean isIncludeB2, boolean isIncludeC, boolean isIncludeDEF, boolean questionDie, String awsa1, int ent) {
        this.questionId = questionId;
        this.questionContent = questionContent;
        this.questionImg = questionImg;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.answerDescription = answerDescription;
        this.answer = answer;
        this.questionType = questionType;
        this.isLearned = isLearned;
        this.mark = mark;
        this.isWrong = isWrong;
        this.isIncludeA1 = isIncludeA1;
        this.isIncludeA2 = isIncludeA2;
        this.isIncludeA34 = isIncludeA34;
        this.isIncludeB1 = isIncludeB1;
        this.isIncludeB2 = isIncludeB2;
        this.isIncludeC = isIncludeC;
        this.isIncludeDEF = isIncludeDEF;
        this.questionDie = questionDie;
        this.awsa1 = awsa1;
        this.ent = ent;
    }

    public Question(int questionId, String questionContent, String questionImg, String option1, String option2, String option3, String option4, String answerDescription, int answer, boolean isLearned, int mark, boolean isWrong) {
        this.questionId = questionId;
        this.questionContent = questionContent;
        this.questionImg = questionImg;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.answerDescription = answerDescription;
        this.answer = answer;
        this.isLearned = isLearned;
        this.mark = mark;
        this.isWrong = isWrong;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getQuestionImg() {
        return questionImg;
    }

    public void setQuestionImg(String questionImg) {
        this.questionImg = questionImg;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public String getAnswerDescription() {
        return answerDescription;
    }

    public void setAnswerDescription(String answerDescription) {
        this.answerDescription = answerDescription;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public int getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    public boolean isLearned() {
        return isLearned;
    }

    public void setLearned(boolean learned) {
        isLearned = learned;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public boolean isWrong() {
        return isWrong;
    }

    public void setWrong(boolean wrong) {
        isWrong = wrong;
    }

    public boolean isIncludeA1() {
        return isIncludeA1;
    }

    public void setIncludeA1(boolean includeA1) {
        isIncludeA1 = includeA1;
    }

    public boolean isIncludeA2() {
        return isIncludeA2;
    }

    public void setIncludeA2(boolean includeA2) {
        isIncludeA2 = includeA2;
    }

    public boolean isIncludeA34() {
        return isIncludeA34;
    }

    public void setIncludeA34(boolean includeA34) {
        isIncludeA34 = includeA34;
    }

    public boolean isIncludeB1() {
        return isIncludeB1;
    }

    public void setIncludeB1(boolean includeB1) {
        isIncludeB1 = includeB1;
    }

    public boolean isIncludeB2() {
        return isIncludeB2;
    }

    public void setIncludeB2(boolean includeB2) {
        isIncludeB2 = includeB2;
    }

    public boolean isIncludeC() {
        return isIncludeC;
    }

    public void setIncludeC(boolean includeC) {
        isIncludeC = includeC;
    }

    public boolean isIncludeDEF() {
        return isIncludeDEF;
    }

    public void setIncludeDEF(boolean includeDEF) {
        isIncludeDEF = includeDEF;
    }

    public boolean isQuestionDie() {
        return questionDie;
    }

    public void setQuestionDie(boolean questionDie) {
        this.questionDie = questionDie;
    }

    public String getAwsa1() {
        return awsa1;
    }

    public void setAwsa1(String awsa1) {
        this.awsa1 = awsa1;
    }

    public int getEnt() {
        return ent;
    }

    public void setEnt(int ent) {
        this.ent = ent;
    }
}
