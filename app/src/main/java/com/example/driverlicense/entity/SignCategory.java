package com.example.driverlicense.entity;

import java.io.Serializable;

public class SignCategory implements Serializable {
    private int signCatePk;
    private int ent;
    private int opt;
    private int index;
    private String signCateName;

    public SignCategory(int signCatePk, int ent, int opt, int index, String signCateName) {
        this.signCatePk = signCatePk;
        this.ent = ent;
        this.opt = opt;
        this.index = index;
        this.signCateName = signCateName;
    }

    public SignCategory() {
    }

    public int getSignCatePk() {
        return signCatePk;
    }

    public void setSignCatePk(int signCatePk) {
        this.signCatePk = signCatePk;
    }

    public int getEnt() {
        return ent;
    }

    public void setEnt(int ent) {
        this.ent = ent;
    }

    public int getOpt() {
        return opt;
    }

    public void setOpt(int opt) {
        this.opt = opt;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getSignCateName() {
        return signCateName;
    }

    public void setSignCateName(String signCateName) {
        this.signCateName = signCateName;
    }
}
