package com.example.driverlicense.entity;

import java.io.Serializable;

public class QuestionType implements Serializable {
    private int questTypeId;
    private int ent;
    private int opt;
    private String description;
    private String id;
    private String typeName;
    private int currentIndex;
    private int numOfQuesiton;
    private int numOfDieQuestion;

    public QuestionType(int questTypeId, int ent, int opt, String description, String id, String typeName, int currentIndex) {
        this.questTypeId = questTypeId;
        this.ent = ent;
        this.opt = opt;
        this.description = description;
        this.id = id;
        this.typeName = typeName;
        this.currentIndex = currentIndex;
    }

    public QuestionType() {
    }

    public int getNumOfQuesiton() {
        return numOfQuesiton;
    }

    public void setNumOfQuesiton(int numOfQuesiton) {
        this.numOfQuesiton = numOfQuesiton;
    }

    public int getNumOfDieQuestion() {
        return numOfDieQuestion;
    }

    public void setNumOfDieQuestion(int numOfdieQuestion) {
        this.numOfDieQuestion = numOfdieQuestion;
    }

    public int getQuestTypeId() {
        return questTypeId;
    }

    public void setQuestTypeId(int questTypeId) {
        this.questTypeId = questTypeId;
    }

    public int getEnt() {
        return ent;
    }

    public void setEnt(int ent) {
        this.ent = ent;
    }

    public int getOpt() {
        return opt;
    }

    public void setOpt(int opt) {
        this.opt = opt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }
}
