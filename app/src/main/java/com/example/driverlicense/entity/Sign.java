package com.example.driverlicense.entity;

import java.io.Serializable;

public class Sign implements Serializable {
    private int signPk;
    private int ent;
    private int opt;
    private int signCatePk;
    private String signDesc;
    private String signImg;
    private int imageIndex;
    private String signName;

    public Sign() {
    }

    public Sign(int signPk, int ent, int opt, int signCatePk, String signDesc, String signImg, int imageIndex, String signName) {
        this.signPk = signPk;
        this.ent = ent;
        this.opt = opt;
        this.signCatePk = signCatePk;
        this.signDesc = signDesc;
        this.signImg = signImg;
        this.imageIndex = imageIndex;
        this.signName = signName;
    }

    public int getSignPk() {
        return signPk;
    }

    public void setSignPk(int signPk) {
        this.signPk = signPk;
    }

    public int getEnt() {
        return ent;
    }

    public void setEnt(int ent) {
        this.ent = ent;
    }

    public int getOpt() {
        return opt;
    }

    public void setOpt(int opt) {
        this.opt = opt;
    }

    public int getSignCatePk() {
        return signCatePk;
    }

    public void setSignCatePk(int signCatePk) {
        this.signCatePk = signCatePk;
    }

    public String getSignDesc() {
        return signDesc;
    }

    public void setSignDesc(String signDesc) {
        this.signDesc = signDesc;
    }

    public String getSignImg() {
        return signImg;
    }

    public void setSignImg(String signImg) {
        this.signImg = signImg;
    }

    public int getImageIndex() {
        return imageIndex;
    }

    public void setImageIndex(int imageIndex) {
        this.imageIndex = imageIndex;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }
}
