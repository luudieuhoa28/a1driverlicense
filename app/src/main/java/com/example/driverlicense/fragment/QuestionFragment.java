package com.example.driverlicense.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.driverlicense.R;
import com.example.driverlicense.database.QuestionDatabaseAccess;
import com.example.driverlicense.database.QuestionTypeDatabaseAccess;
import com.example.driverlicense.database.TestQuestionDatabaseAccess;
import com.example.driverlicense.entity.Option;
import com.example.driverlicense.entity.Question;
import com.example.driverlicense.adapter.QuestionBeforeTestAdapter;
import com.example.driverlicense.entity.QuestionType;
import com.example.driverlicense.entity.Test;
import com.example.driverlicense.entity.TestQuestion;

import java.util.ArrayList;
import java.util.List;

//show the full question: question content, question options, question image
//handing the choose answer action
public class QuestionFragment extends Fragment {

    private static List<Question> listTestQuestion = new ArrayList<>();
    private int typeAction;

    public static QuestionFragment newInstance(Question question, TestQuestion testQuestion, Test test, int typeAction) {
        QuestionFragment fragment = new QuestionFragment();
        Bundle args = new Bundle();
        args.putSerializable("QUESTION", question);
        args.putSerializable("QUESTION_TEST", testQuestion);
        args.putSerializable("TEST", test);
        args.putInt("TYPE_ACTION", typeAction);
        fragment.setArguments(args);
        return fragment;
    }

    public static QuestionFragment newInstance(Question question, QuestionType questionType, int typeAction) {
        QuestionFragment fragment = new QuestionFragment();
        Bundle args = new Bundle();
        args.putSerializable("QUESTION", question);
        args.putInt("TYPE_ACTION", typeAction);
        args.putSerializable("QUESTION_TYPE", questionType);
        fragment.setArguments(args);
        return fragment;
    }


    public static List<Question> getListTestQuestion() {
        return listTestQuestion;
    }

    public static void setListTestQuestion(List<Question> listTestQuestion) {
        QuestionFragment.listTestQuestion = listTestQuestion;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View questionView = inflater.inflate(R.layout.do_test_fragment, container, false);
        TextView questionTextView = questionView.findViewById(R.id.questionTextView);
        ImageView questionImageView = questionView.findViewById(R.id.questionImageView);
        final RecyclerView listAnswer = questionView.findViewById(R.id.listAnswer);
        final TextView explainTitle = questionView.findViewById(R.id.txtExplainTitle);
        final TextView explainQuestion = questionView.findViewById(R.id.txtExplainQuestion);

        final int typeAction = getArguments().getInt("TYPE_ACTION");
        final Question question = (Question) getArguments().getSerializable("QUESTION");
        final QuestionType questionType = (QuestionType) getArguments().getSerializable("QUESTION_TYPE");
        Test test = new Test();
        final TestQuestion testQuestion;
        if (typeAction == 2) {
            testQuestion = (TestQuestion) getArguments().getSerializable("QUESTION_TEST");
            test = (Test) getArguments().getSerializable("TEST");
            if (test.isFinish()) {
                explainTitle.setText("Giải thích đáp án");
                explainQuestion.setText(question.getAnswerDescription());
            } else {
                explainTitle.setText("");
                explainQuestion.setText("");
            }
        } else {
            testQuestion = new TestQuestion();
            if (question.getMark() == question.getAnswer()) {
                explainTitle.setText("Giải thích đáp án");
                explainQuestion.setText(question.getAnswerDescription());
            } else {
                explainTitle.setText("");
                explainQuestion.setText("");
            }
        }


        questionTextView.setText(question.getQuestionContent());
        if (!question.getQuestionImg().equals("")) {
            //this is for test image display
            Uri otherPath = Uri.parse("android.resource://com.example.driverlicense/drawable/a");
            //  questionImageView.setImageURI("android.resource://com.example.driverlicense/drawable/" + Uri.parse(question.getQuestionImg()));
            questionImageView.setImageURI(otherPath);
        } else {
            questionImageView.setImageResource(0);
        }
        final List<Option> listOption = new ArrayList<>();

        if (typeAction == 2) {
            listOption.add(new Option("A", question.getOption1(), testQuestion.getAnswer() == 1 ? true : false, false, false));
            listOption.add(new Option("B", question.getOption2(), testQuestion.getAnswer() == 2 ? true : false, false, false));
            if (question.getOption3() != null) {
                listOption.add(new Option("C", question.getOption3(), testQuestion.getAnswer() == 3 ? true : false, false, false));
            }
            if (question.getOption4() != null) {
                listOption.add(new Option("D", question.getOption4(), testQuestion.getAnswer() == 4 ? true : false, false, false));
            }
        } else {
            listOption.add(new Option("A", question.getOption1(), question.getMark() == 1 ? true : false, question.getAnswer() == 1 ? true : false, false));
            listOption.add(new Option("B", question.getOption2(), question.getMark() == 2 ? true : false, question.getAnswer() == 2 ? true : false, false));
            if (question.getOption3() != null) {
                listOption.add(new Option("C", question.getOption3(), question.getMark() == 3 ? true : false, question.getAnswer() == 3 ? true : false, false));
            }
            if (question.getOption4() != null) {
                listOption.add(new Option("D", question.getOption4(), question.getMark() == 4 ? true : false, question.getAnswer() == 4 ? true : false, false));
            }

        }

        //update when user choose the answer
        final QuestionBeforeTestAdapter questionBeforeTestAdapter;
        if (typeAction == 2) {
           questionBeforeTestAdapter = new QuestionBeforeTestAdapter(listOption, question, test, testQuestion, typeAction);

        } else {
            questionBeforeTestAdapter = new QuestionBeforeTestAdapter(listOption, question, typeAction);
        }

        questionBeforeTestAdapter.setOnItemClickListener(new QuestionBeforeTestAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                if (typeAction == 2) {
                    if (testQuestion.getAnswer() == position + 1) {
                        testQuestion.setAnswer(0);
                    } else {
                        testQuestion.setAnswer(position + 1);
                }
                } else {
                    int tmpMark = question.getMark();
                    if (question.getMark() == position + 1) {
                        question.setMark(0);
                        explainTitle.setText("");
                        explainQuestion.setText("");
                    } else {
                        question.setMark(position + 1);
                        if (listOption.get(position).isTrue()) {
                            explainTitle.setText("Giải thích đáp án");
                            explainQuestion.setText(question.getAnswerDescription());
                        } else {
                            explainTitle.setText("");
                            explainQuestion.setText("");
                        }
                    }
                    if (tmpMark > 0  && question.getMark() == 0) {
                        questionType.setCurrentIndex(questionType.getCurrentIndex() - 1);
                    } else if (tmpMark == 0 && question.getMark() > 0){
                        questionType.setCurrentIndex(questionType.getCurrentIndex() + 1);
                    }
                }


                for (int i = 0; i < listOption.size(); i++) {
                    if (i == position) {
                        if (listOption.get(i).isSelected()) {
                            listOption.get(i).setSelected(false);
                        } else {
                            listOption.get(i).setSelected(true);
                        }

                    } else {
                        listOption.get(i).setSelected(false);
                    }
                }
                questionBeforeTestAdapter.notifyItemChanged(position);
                questionBeforeTestAdapter.notifyDataSetChanged();
                if (typeAction == 2) {
                    TestQuestionDatabaseAccess testQuestionDatabaseAccess = TestQuestionDatabaseAccess.getInstance(getContext());
                    testQuestionDatabaseAccess.open();
                    testQuestionDatabaseAccess.updateTestQuest(testQuestion);
                    testQuestionDatabaseAccess.close();
                } else {
                    QuestionDatabaseAccess questionDatabaseAccess = QuestionDatabaseAccess.getInstance(getContext());
                    questionDatabaseAccess.open();
                    questionDatabaseAccess.updateQuestion(question);
                    questionDatabaseAccess.close();

                    QuestionTypeDatabaseAccess questionTypeDatabaseAccess = QuestionTypeDatabaseAccess.getInstance(getContext());
                    questionTypeDatabaseAccess.open();
                    questionTypeDatabaseAccess.updateQuestionType(questionType);
                    questionTypeDatabaseAccess.close();
                }


                RecyclerView listStatusView= (RecyclerView) getActivity().findViewById(R.id.listStatusQuestion);
                if (listStatusView.getAdapter() != null) {
                    listStatusView.getAdapter().notifyDataSetChanged();
                }
            }
        });
        listAnswer.setLayoutManager(new LinearLayoutManager(getActivity()));
        listAnswer.setAdapter(questionBeforeTestAdapter);
        Toast.makeText(this.getContext(), "create view ne", Toast.LENGTH_SHORT).show();
        return questionView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

}
