package com.example.driverlicense.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.driverlicense.R;
import com.example.driverlicense.adapter.SignAdapter;
import com.example.driverlicense.database.SignDatabaseAccess;
import com.example.driverlicense.entity.Sign;
import com.example.driverlicense.entity.SignCategory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SignFragment extends Fragment {
    static List<Sign> signList;
    static SignCategory signCategory;

    public static SignFragment newInstance(SignCategory signCategory) {
        SignFragment fragment = new SignFragment();
        Bundle args = new Bundle();
        args.putSerializable("SIGN_CATE", signCategory);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View signView = inflater.inflate(R.layout.sign_fragment, container, false);
        RecyclerView signRecyclerview = signView.findViewById(R.id.signRecyclerview);
        SignCategory signCategory = (SignCategory) getArguments().getSerializable("SIGN_CATE");

        //get sign list from db
        SignDatabaseAccess signDatabaseAccess = SignDatabaseAccess.getInstance(getContext());
        signDatabaseAccess.open();
        signList = signDatabaseAccess.getListSignByCateId(signCategory.getSignCatePk());
        signDatabaseAccess.close();
        SignAdapter signAdapter = new SignAdapter(signList);
        signRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        signRecyclerview.setAdapter(signAdapter);
        return signView;
    }
}
