package com.example.driverlicense.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.driverlicense.database.TestDatabaseAccess;
import com.example.driverlicense.entity.Question;
import com.example.driverlicense.entity.Test;

import java.util.ArrayList;
import java.util.List;

public class TestViewModel extends AndroidViewModel {

       private List<Test> listTest = new ArrayList<>();
    TestDatabaseAccess testDatabaseAccess;
    //List<Test> getListTest();
    public TestViewModel(@NonNull Application application) {
        super(application);
        testDatabaseAccess = TestDatabaseAccess.getInstance(application.getApplicationContext());

    }
    public List<Test> getListTest() {
        testDatabaseAccess.open();
        listTest = testDatabaseAccess.getListTest("A1");

        testDatabaseAccess.close();
        return listTest;
    }

    public void setListTest(List<Test> listTest) {
        this.listTest = listTest;
    }

}
