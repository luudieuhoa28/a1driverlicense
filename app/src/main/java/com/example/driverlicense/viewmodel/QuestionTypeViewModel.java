package com.example.driverlicense.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.driverlicense.database.QuestionTypeDatabaseAccess;
import com.example.driverlicense.entity.QuestionType;

import java.util.ArrayList;
import java.util.List;


public class QuestionTypeViewModel extends AndroidViewModel {
    private List<QuestionType> questionTypeList = new ArrayList<>();
    QuestionTypeDatabaseAccess questionTypeDatabaseAccess;
    public QuestionTypeViewModel(@NonNull Application application) {
        super(application);
        questionTypeDatabaseAccess = QuestionTypeDatabaseAccess.getInstance(application.getApplicationContext());

    }

    public List<QuestionType> getQuestionTypeList() {
        questionTypeDatabaseAccess.open();
        questionTypeList = questionTypeDatabaseAccess.getListQuestionType();
        questionTypeDatabaseAccess.close();
        return questionTypeList;
    }

    public void setQuestionTypeList(List<QuestionType> questionTypeList) {
        this.questionTypeList = questionTypeList;
    }
}
