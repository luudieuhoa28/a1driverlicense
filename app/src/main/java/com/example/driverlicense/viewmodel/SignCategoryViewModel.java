package com.example.driverlicense.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.driverlicense.database.SignCategoryDatabaseAccess;
import com.example.driverlicense.entity.SignCategory;

import java.util.ArrayList;
import java.util.List;

public class SignCategoryViewModel extends AndroidViewModel {
    List<SignCategory> signCategoryList = new ArrayList<>();
    SignCategoryDatabaseAccess signCategoryDatabaseAccess;

    public SignCategoryViewModel(@NonNull Application application) {
        super(application);
        signCategoryDatabaseAccess = SignCategoryDatabaseAccess.getInstance(application.getApplicationContext());
    }

    public List<SignCategory> getSignCategoryList() {
        signCategoryDatabaseAccess.open();
        signCategoryList = signCategoryDatabaseAccess.getListSignCate();
        signCategoryDatabaseAccess.close();
        return signCategoryList;
    }

    public void setSignCategoryList(List<SignCategory> signCategoryList) {
        this.signCategoryList = signCategoryList;
    }
}
