package com.example.driverlicense.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.driverlicense.entity.QuestionType;
import com.example.driverlicense.entity.SignCategory;

import java.util.ArrayList;
import java.util.List;

public class SignCategoryDatabaseAccess {
    private DatabaseOpenHelper openHelper;
    private SQLiteDatabase db;
    private static SignCategoryDatabaseAccess instance;

    private static final String TAG = "SQLite";
    private static final String TABLE_SIGN_CATE = "ZSIGNCATEGORY";

    public SignCategoryDatabaseAccess(Context context) {
        openHelper = new DatabaseOpenHelper(context);
    }


    public static SignCategoryDatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new SignCategoryDatabaseAccess(context);
        } return instance;
    }

    public void open() {
        this.db = openHelper.getWritableDatabase();
    }

    public void  close() {
        if(db != null) {
            this.db.close();
        }
    }

    public List<SignCategory> getListSignCate() {
        List<SignCategory> listSignCate = new ArrayList<>();
        Cursor cursor = null;
        try{
            String query = "SELECT * FROM " + TABLE_SIGN_CATE;
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do{
                    SignCategory signCategory = new SignCategory();
                    try {
                        signCategory.setSignCatePk(Integer.parseInt(cursor.getString(0)));
                        signCategory.setSignCateName(cursor.getString(4));
                        listSignCate.add(signCategory);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return listSignCate;
    }
}
