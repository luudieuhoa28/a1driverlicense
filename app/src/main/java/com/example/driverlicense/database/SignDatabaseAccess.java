package com.example.driverlicense.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.driverlicense.entity.Sign;
import com.example.driverlicense.entity.SignCategory;

import java.util.ArrayList;
import java.util.List;

public class SignDatabaseAccess  {

    private DatabaseOpenHelper openHelper;
    private SQLiteDatabase db;
    private static SignDatabaseAccess instance;

    private static final String TAG = "SQLite";
    private static final String TABLE_SIGN = "ZSIGN";
    private static final String COLUMN_SIGN_CATE = "ZSIGNCATEGORY";

    public SignDatabaseAccess(Context context) {
        openHelper = new DatabaseOpenHelper(context);
    }


    public static SignDatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new SignDatabaseAccess(context);
        } return instance;
    }

    public void open() {
        this.db = openHelper.getWritableDatabase();
    }

    public void  close() {
        if(db != null) {
            this.db.close();
        }
    }

    public List<Sign> getListSignByCateId(int signCatePk) {
        List<Sign> listSign = new ArrayList<>();
        Cursor cursor = null;
        try{
            String query = "SELECT * FROM " + TABLE_SIGN + " WHERE " + COLUMN_SIGN_CATE + " = " + signCatePk ;
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do{
                    Sign sign = new Sign();
                    try {
                        sign.setSignPk(Integer.parseInt(cursor.getString(0)));
                        sign.setSignCatePk(Integer.parseInt(cursor.getString(3)));
                        sign.setSignDesc(cursor.getString(4));
                        sign.setSignImg(cursor.getString(5));
                        sign.setSignName(cursor.getString(7));
                        listSign.add(sign);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {

        }
        return listSign;
    }
}
