package com.example.driverlicense.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.driverlicense.entity.QuestionType;
import com.example.driverlicense.entity.Test;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.List;

public class QuestionTypeDatabaseAccess {
    private DatabaseOpenHelper openHelper;
    private SQLiteDatabase db;
    private static QuestionTypeDatabaseAccess instance;

    private static final String TAG = "SQLite";
    private static final String TABLE_QUESTION_TYPE = "ZQUESTIONTYPE";
    private static final String COLUMN_PK = "Z_PK";
    private static final String COLUMN_CURRENT_INDEX = "ZCURRENT_INDEX";
    private static final String COLUMN_NUM_OF_QUESTION = "NUMBER_OF_QUESTION";
    private static final String COLUMN_NUM_OF_DIE_QUESTION = "NUMBER_OF_TYPE_QUESTION";

    public QuestionTypeDatabaseAccess(Context context) {
        openHelper = new DatabaseOpenHelper(context);
    }


    public static QuestionTypeDatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new QuestionTypeDatabaseAccess(context);
        } return instance;
    }

    public void open() {
        this.db = openHelper.getWritableDatabase();
    }

    public void  close() {
        if(db != null) {
            this.db.close();
        }
    }

    public List<QuestionType> getListQuestionType() {
        List<QuestionType> listQuestionType = new ArrayList<>();
        Cursor cursor = null;
        try{
            String query = "SELECT * FROM " + TABLE_QUESTION_TYPE;
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do{
                    QuestionType questionType = new QuestionType();
                    try {
                        questionType.setQuestTypeId(Integer.parseInt(cursor.getString(0)));
                        questionType.setDescription(cursor.getString(3));
                        questionType.setTypeName(cursor.getString(5));
                        questionType.setCurrentIndex(Integer.parseInt(cursor.getString(6)));
                        questionType.setNumOfQuesiton(Integer.parseInt(cursor.getString(7)));
                        questionType.setNumOfDieQuestion(Integer.parseInt(cursor.getString(8)));
                        listQuestionType.add(questionType);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {

        }
        return listQuestionType;
    }

    //this is call when user choose answer in "hoc"
    public int updateQuestionType(QuestionType questionType) {
        Log.i(TAG, "QuestionTypeDatabaseAccess.updateQuestionType ... " + questionType.getQuestTypeId());
        ContentValues values = new ContentValues();
        values.put(COLUMN_CURRENT_INDEX, questionType.getCurrentIndex());

        // updating row
        int result = db.update(TABLE_QUESTION_TYPE, values, COLUMN_PK + " = ?",
                new String[]{String.valueOf(questionType.getQuestTypeId())});
        return result;
    }

}
