package com.example.driverlicense.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.driverlicense.adapter.TestStatusAdapter;
import com.example.driverlicense.database.TestDatabaseAccess;
import com.example.driverlicense.database.DatabasePreparing;
import com.example.driverlicense.database.TestQuestionDatabaseAccess;
import com.example.driverlicense.entity.Question;
import com.example.driverlicense.adapter.QuestionPagerAdapter;
import com.example.driverlicense.entity.QuestionType;
import com.example.driverlicense.entity.TestQuestion;
import com.example.driverlicense.viewmodel.QuestionViewModel;
import com.example.driverlicense.R;
import com.example.driverlicense.entity.Test;
import com.example.driverlicense.viewmodel.TestQuestionViewModel;

import java.util.List;

public class DoTestActivity extends AppCompatActivity {
    QuestionViewModel questionViewModel;
    private static List<Question> listQuestion;
    private List<TestQuestion> listTestQuestion;
    TestQuestionViewModel testQuestionViewModel;
    ViewPager questionViewPager;
    QuestionPagerAdapter questionPagerAdapter;
    RecyclerView listStatusQuestion;
    TestStatusAdapter testStatusAdapter;
    CountDownTimer countDownTimer;
    Test test;
    DoTestActivity doTestActivity;
    boolean isCountTimerRunning = false;
    TextView txtOrderQuest;
    int typeAction;
    QuestionType questionType;
    MenuItem menuItem;


    public QuestionViewModel getQuestionViewModel() {
        return questionViewModel;
    }

    public void setQuestionViewModel(QuestionViewModel questionViewModel) {
        this.questionViewModel = questionViewModel;
    }

    public static List<Question> getListQuestion() {
        return listQuestion;
    }

    public static void setListQuestion(List<Question> listQuestion) {
        DoTestActivity.listQuestion = listQuestion;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_test);
        doTestActivity = this;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtOrderQuest = findViewById(R.id.txtOrderQuest);
        Intent intent = getIntent();
        Bundle data = intent.getBundleExtra("DATA_BUNDLE");
        //1
        typeAction = data.getInt("TYPE_ACTION");
        if (typeAction == 2) {
            test = (Test) data.getSerializable("TEST_DATA");
            setTitle("Bộ đề số " + test.getNameTest());
            startTimerCounter();
            txtOrderQuest.setText("Câu 1/25");
        } else {
            questionType = (QuestionType) data.getSerializable("QUESTION_TYPE_DATA");
            txtOrderQuest.setText("Câu 1/" + questionType.getNumOfQuesiton());
        }


        //1
        if (typeAction == 2) {
            testQuestionViewModel = ViewModelProviders.of(this).get(TestQuestionViewModel.class);
        }

        //1
        //get the test question from the testId from db
        if (typeAction == 2) {
            listTestQuestion = testQuestionViewModel.getTestQuestionList(test.getIdTest());
        }

        //còn cái list này thì lấy dựa vào hàm gọi getListQuestionFromDb(List<TestQuestion> listQuestionTest);
        questionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);
        if (typeAction == 2) {
            listQuestion = questionViewModel.getListQuest(listTestQuestion);
        } else {
            listQuestion = questionViewModel.getListQuest(questionType.getQuestTypeId());
        }

        //1
        if (typeAction == 2) {
            questionPagerAdapter = new QuestionPagerAdapter(getSupportFragmentManager(), listQuestion, listTestQuestion, test, typeAction);
        } else {
            questionPagerAdapter = new QuestionPagerAdapter(getSupportFragmentManager(), listQuestion, questionType, typeAction);
        }
        questionViewPager = (ViewPager) findViewById(R.id.testQuesViewPager);
        questionViewPager.setAdapter(questionPagerAdapter);
        questionViewPager.setOnPageChangeListener(pageChangeListener);

        listStatusQuestion = findViewById(R.id.listStatusQuestion);
        if (typeAction == 2) {
            testStatusAdapter = new TestStatusAdapter(listTestQuestion, typeAction);

        } else {
            testStatusAdapter = new TestStatusAdapter(typeAction, listQuestion);
        }
        listStatusQuestion.setLayoutManager(new GridLayoutManager(this, 4));

        testStatusAdapter.setOnItemClickListener(new TestStatusAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                questionViewPager.setCurrentItem(position);
            }
        });
        listStatusQuestion.setAdapter(testStatusAdapter);
    }


    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

        int currentPosition = 0;

        @Override
        public void onPageSelected(int newPosition) {
            questionPagerAdapter.getItem(currentPosition).onPause();

            questionPagerAdapter.getItem(newPosition).onResume();

            currentPosition = newPosition;
            TextView txtOrderQuest = findViewById(R.id.txtOrderQuest);
            if (typeAction == 2) {
                txtOrderQuest.setText((currentPosition + 1) + "/25");
            } else {
                txtOrderQuest.setText((currentPosition + 1) + "/" +questionType.getNumOfQuesiton());
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.do_test, menu);
        if (typeAction == 2) {
            if (test.isFinish()) {
                menu.getItem(0).setTitle("Làm lại");
            } else {
                menu.getItem(0).setTitle("Kết thúc");
            }
        } else {
            menu.getItem(0).setTitle("");
        }
        return true;
    }

    //https://stackoverflow.com/questions/2115758/how-do-i-display-an-alert-dialog-on-android
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        menuItem = item;
        final boolean[] result = {false};
        pauseTest();

        switch (menuItem.getItemId()) {
            case R.id.doTest:
                new AlertDialog.Builder(doTestActivity)
                        .setTitle("Lưu ý")
                        .setMessage("Bạn có chắc muốn " + menuItem.getTitle() + " không?")
                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(R.string.yes_option, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                                if (typeAction == 2) {
                                    if (test.isFinish()) {
                                        beginTest();
                                        menuItem.setTitle("Kết thúc");
                                    } else {
                                        endTest();
                                        menuItem.setTitle("Làm lại");
                                    }
                                }
                            }
                        })
                        // Set action for 'no' option
                        .setNegativeButton(R.string.no_option, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                restartTest();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;
            case android.R.id.home:
                if (typeAction == 2) {
                    if (!test.isFinish()) {
                        pauseTest();
                        new AlertDialog.Builder(this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Tạm dừng")
                                .setMessage("Bạn có chắc muốn tạm dừng bài làm và tiếp tục sau hay không?")
                                .setPositiveButton(R.string.yes_option, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setNegativeButton(R.string.no_option, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        restartTest();
                                    }
                                })
                                .show();
                        return true;
                    }
                }
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void beginTest() {

        //reset test status
        test.setFinish(false);
        test.setFailed(false);
        test.setTotalSuccess(0);
        test.setCurrentTime(30000);

        //reset testquestion status
        listTestQuestion = testQuestionViewModel.resetTestQuest();
        //chỗ này làm sao để thông báo cho người ta biết là cái testquest có thay đổi nè
        testStatusAdapter.notifyDataSetChanged();

        //reset all question
        questionViewPager.setAdapter(questionPagerAdapter);
        //notify the data change
        RecyclerView answerList = findViewById(R.id.listAnswer);
        answerList.getAdapter().notifyDataSetChanged();
        questionViewPager.setCurrentItem(0);
        txtOrderQuest.setText("1/25");
        startTimerCounter();
    }

    private void pauseTest() {
        if (typeAction == 2) {
            stopTimerCounter();
        }
    }

    private void restartTest() {
        if (typeAction == 2) {
            startTimerCounter();
        }
    }

    private void endTest() {
        stopTimerCounter();
        test.setFinish(true);
        //set fail hay pass(xem laij ham nay)
        int numOfCorrectQuestion = test.checkFail(listQuestion, listTestQuestion);
        if (numOfCorrectQuestion >= 21) {
            test.setFailed(false);
        } else {
            test.setFailed(true);
        }
        //set number of correct question
        test.setTotalSuccess(numOfCorrectQuestion);
        //reset all question
        questionViewPager.setAdapter(questionPagerAdapter);

        //notify the data change
        RecyclerView answerList = findViewById(R.id.listAnswer);
        answerList.getAdapter().notifyDataSetChanged();
        questionViewPager.setCurrentItem(0);
        txtOrderQuest.setText("1/25");
        if (menuItem != null) {
            menuItem.setTitle("Làm lại");
        }

    }

    //comment for testing the back icon onclick function
//    @Override
//    protected void onPause() {
//        if (typeAction == 2) {
//            stopTimerCounter();
//        }
//        super.onPause();
//    }

    public void startTimerCounter() {
        if (test.getCurrentTime() - 1000 >= 0 && !test.isFinish()) {
            countDownTimer = new CountDownTimer(test.getCurrentTime(), 1000) {
                @Override
                public void onTick(long l) {
                    test.setCurrentTime((int) l);
                    updateTimer(test.getCurrentTime());
                    isCountTimerRunning = true;
                }

                @Override
                public void onFinish() {
                    endTest();
                    isCountTimerRunning = false;
                }
            }.start();
        } else {
            updateTimer(test.getCurrentTime());
        }
    }

    @Override
    public void onBackPressed() {
        if (isCountTimerRunning) {
            pauseTest();
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Tạm dừng")
                    .setMessage("Bạn có chắc muốn tạm dừng bài làm và tiếp tục sau hay không?")
                    .setPositiveButton(R.string.yes_option, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.no_option, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            restartTest();
                        }
                    })
                    .show();
        } else {
            finish();
        }
    }

    public void stopTimerCounter() {
        if (isCountTimerRunning) {
            countDownTimer.cancel();
            isCountTimerRunning = false;
        }
        TestDatabaseAccess testDatabaseAccess = TestDatabaseAccess.getInstance(doTestActivity);
        testDatabaseAccess.open();
        testDatabaseAccess.updateTest(test);
        testDatabaseAccess.close();
    }

    public void updateTimer(int timeLeftMiniSecond) {
        int minu = (int) (timeLeftMiniSecond / 60000);
        int second = (int) (timeLeftMiniSecond % 60000 / 1000);

        String timer = "" + minu;
        timer += ":";
        if (second < 10) {
            timer += "0";
        }
        timer += second;
        TextView txtTimer = findViewById(R.id.txtTimer);
        txtTimer.setText(timer);
    }


}