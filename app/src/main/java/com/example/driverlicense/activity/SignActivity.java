package com.example.driverlicense.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.driverlicense.R;
import com.example.driverlicense.adapter.SignPagerAdapter;
import com.example.driverlicense.viewmodel.SignCategoryViewModel;
import com.google.android.material.tabs.TabLayout;

public class SignActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager signViewPager;
    private SignPagerAdapter signPagerAdapter;
    private SignCategoryViewModel signCategoryViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        signViewPager = (ViewPager) findViewById(R.id.signViewpager);
        //setupViewPager(viewPager);
        signCategoryViewModel = ViewModelProviders.of(this).get(SignCategoryViewModel.class);
        signPagerAdapter = new SignPagerAdapter(getSupportFragmentManager(), signCategoryViewModel.getSignCategoryList());
        signViewPager.setAdapter(signPagerAdapter);
        signViewPager.setCurrentItem(0);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(signViewPager);

    }

//    private void setupViewPager(ViewPager viewPager) {
//
//
//    }
//
//    class ViewPagerAdapter extends FragmentPagerAdapter {
//        private final List<Fragment> mFragmentList = new ArrayList<>();
//        private final List<String> mFragmentTitleList = new ArrayList<>();
//
//        public ViewPagerAdapter(FragmentManager manager) {
//            super(manager);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return mFragmentList.get(position);
//        }
//
//        @Override
//        public int getCount() {
//            return mFragmentList.size();
//        }
//
//        public void addFragment(Fragment fragment, String title) {
//            mFragmentList.add(fragment);
//            mFragmentTitleList.add(title);
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
//        }
    }
