package com.example.driverlicense.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.driverlicense.R;
import com.example.driverlicense.adapter.QuestionTypeAdapter;
import com.example.driverlicense.entity.QuestionType;
import com.example.driverlicense.viewmodel.QuestionTypeViewModel;

public class LearnSetActivity extends AppCompatActivity {
    QuestionTypeAdapter questionTypeAdapter;
    QuestionTypeViewModel questionTypeViewModel;
    RecyclerView questTypeRecycleView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_set);
        questionTypeViewModel = ViewModelProviders.of(this).get(QuestionTypeViewModel.class);
        questionTypeAdapter = new QuestionTypeAdapter(questionTypeViewModel.getQuestionTypeList());
        questTypeRecycleView = findViewById(R.id.learnSetRecyclerView);
        questTypeRecycleView.setAdapter(questionTypeAdapter);
        questTypeRecycleView.setLayoutManager(new LinearLayoutManager(this));
        questionTypeAdapter.setOnItemClickListener(new QuestionTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(QuestionType questionType) {
                Intent intent = new Intent(LearnSetActivity.this, DoTestActivity.class);
                Bundle testData = new Bundle();
                testData.putSerializable("QUESTION_TYPE_DATA", questionType);
                testData.putInt("TYPE_ACTION", 1);
                intent.putExtra("DATA_BUNDLE", testData);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        questionTypeAdapter.notifyDataSetChanged();
        questionTypeAdapter = new QuestionTypeAdapter(questionTypeViewModel.getQuestionTypeList());
        questTypeRecycleView.setAdapter(questionTypeAdapter);
        questionTypeAdapter.setOnItemClickListener(new QuestionTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(QuestionType questionType) {
                Intent intent = new Intent(LearnSetActivity.this, DoTestActivity.class);
                Bundle testData = new Bundle();
                testData.putSerializable("QUESTION_TYPE_DATA", questionType);
                testData.putInt("TYPE_ACTION", 1);
                intent.putExtra("DATA_BUNDLE", testData);
                startActivity(intent);
            }
        });
    }
}