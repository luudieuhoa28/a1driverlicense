package com.example.driverlicense.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.driverlicense.entity.Option;
import com.example.driverlicense.R;
import com.example.driverlicense.entity.Question;
import com.example.driverlicense.entity.Test;
import com.example.driverlicense.entity.TestQuestion;

import java.util.ArrayList;
import java.util.List;

//This adapter is used for every option in a question
public class QuestionBeforeTestAdapter extends RecyclerView.Adapter<QuestionBeforeTestAdapter.QuestionTestViewHoler> {
    List<Option> options = new ArrayList<>();
    private OnItemClickListener listener;
    Question question;
    Test test;
    TestQuestion testQuestion;
    int typeAction;

    public QuestionBeforeTestAdapter(List<Option> options, Question question, Test test, TestQuestion testQuestion, int typeAction) {
        this.options = options;
        this.question = question;
        this.test = test;
        this.testQuestion = testQuestion;
        this.typeAction = typeAction;
    }

    public QuestionBeforeTestAdapter(List<Option> options, Question question, int typeAction) {
        this.options = options;
        this.question = question;
        this.typeAction = typeAction;
    }

    @NonNull
    @Override
    public QuestionTestViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.answer_item, parent, false);
        return new QuestionBeforeTestAdapter.QuestionTestViewHoler(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionTestViewHoler holder, int position) {
        Option option = options.get(position);
        holder.answerOrder.setText(option.getOrder());
        holder.answerContent.setText(option.getContentOption());
        if (typeAction == 2) {
            if(testQuestion.getAnswer() > 0 && testQuestion.getAnswer() == position + 1) {
                holder.answerOrder.setBackgroundColor(Color.GREEN);
                if (test.isFinish()) {
                    if (question.getAnswer() != testQuestion.getAnswer()) {
                        holder.answerContent.setTextColor(Color.RED);
                    } else {
                        holder.answerContent.setTextColor(Color.GREEN);
                    }
                }
            } else {
                if (position + 1 == question.getAnswer() && test.isFinish()){
                    holder.answerContent.setTextColor(Color.GREEN);
                }
                holder.answerOrder.setBackgroundColor(Color.WHITE);
            }
        } else {
            if (question.getMark() > 0 && position + 1 == question.getMark()) {
                if (question.getAnswer() == question.getMark()) {
                    holder.answerOrder.setBackgroundColor(Color.GREEN);
                    holder.answerContent.setTextColor(Color.GREEN);
                } else {
                    holder.answerOrder.setBackgroundColor(Color.RED);
                    holder.answerContent.setTextColor(Color.RED);
                }
            } else {
                holder.answerOrder.setBackgroundColor(Color.WHITE);
                holder.answerContent.setTextColor(Color.BLACK);
            }

        }



    }

    @Override
    public int getItemCount() {
        return options.size();
    }

    public class QuestionTestViewHoler extends RecyclerView.ViewHolder{
        TextView answerOrder;
        TextView answerContent;
        public QuestionTestViewHoler(@NonNull View itemView) {
            super(itemView);
            answerContent = itemView.findViewById(R.id.answerContent);
            answerOrder = itemView.findViewById(R.id.answerOrder);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (typeAction == 2) {
                        if (!test.isFinish()) {
                            int position = getAdapterPosition();
                            if (listener != null && position != RecyclerView.NO_POSITION) {
                                listener.onItemClick(position);
                            }
                        }
                    } else {
                        int position = getAdapterPosition();
                        if (listener != null && position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
