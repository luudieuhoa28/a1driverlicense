package com.example.driverlicense.adapter;

import android.media.Image;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.driverlicense.R;
import com.example.driverlicense.entity.Sign;

import java.util.ArrayList;
import java.util.List;

public class SignAdapter extends RecyclerView.Adapter<SignAdapter.SignViewHolder> {
    List<Sign> signList = new ArrayList<>();

    public SignAdapter(List<Sign> signList) {
        this.signList = signList;
    }

    @NonNull
    @Override
    public SignViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sign_item, parent, false);
        return new SignAdapter.SignViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SignViewHolder holder, int position) {
        holder.signName.setText(signList.get(position).getSignName());
        holder.signDesc.setText(signList.get(position).getSignDesc());
       // thêm hàm set ảnh cho sigImg ở đây
        Uri otherPath = Uri.parse("android.resource://com.example.driverlicense/drawable/sign");
        //  questionImageView.setImageURI("android.resource://com.example.driverlicense/drawable/" + Uri.parse(question.getQuestionImg()));
        holder.signImg.setImageURI(otherPath);

    }

    @Override
    public int getItemCount() {
        return signList.size();
    }

    class SignViewHolder extends RecyclerView.ViewHolder {
        ImageView signImg;
        TextView signName;
        TextView signDesc;
        public SignViewHolder(@NonNull View itemView) {
            super(itemView);
            signImg = itemView.findViewById(R.id.signImg);
            signName = itemView.findViewById(R.id.signName);
            signDesc = itemView.findViewById(R.id.signDesc);
        }
    }
}
