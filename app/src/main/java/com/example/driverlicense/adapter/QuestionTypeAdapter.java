package com.example.driverlicense.adapter;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.driverlicense.R;
import com.example.driverlicense.entity.QuestionType;
import com.example.driverlicense.entity.Test;

import java.util.ArrayList;
import java.util.List;

public class QuestionTypeAdapter extends RecyclerView.Adapter<QuestionTypeAdapter.LearnSetHolder> {
    List<QuestionType> questionTypeList = new ArrayList<>();
    private OnItemClickListener listener;

    public QuestionTypeAdapter(List<QuestionType> questionTypeList) {
        this.questionTypeList = questionTypeList;
    }

    public List<QuestionType> getQuestionTypeList() {
        return questionTypeList;
    }

    public void setQuestionTypeList(List<QuestionType> questionTypeList) {
        this.questionTypeList = questionTypeList;
    }

    @NonNull
    @Override
    public LearnSetHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.learn_set_item, parent, false);
        return new QuestionTypeAdapter.LearnSetHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull LearnSetHolder holder, int position) {
        QuestionType questionType = questionTypeList.get(position);
        holder.typeQuestionName.setText(questionType.getTypeName());
        holder.questTypeDesc.setText(questionType.getDescription());
        holder.numOfLearnedQuest.setText(questionType.getCurrentIndex() + "/" + questionType.getNumOfQuesiton());
        holder.progressBarLearnedQuest.setMax(questionType.getNumOfQuesiton());
        holder.progressBarLearnedQuest.setMin(0);
        holder.progressBarLearnedQuest.setProgress(questionType.getCurrentIndex());
    }

    @Override
    public int getItemCount() {
        return questionTypeList.size();
    }

    class LearnSetHolder extends RecyclerView.ViewHolder{
        private ImageView learnSetImg;
        private TextView typeQuestionName;
        private TextView questTypeDesc;
        private ProgressBar progressBarLearnedQuest;
        private TextView numOfLearnedQuest;
        public LearnSetHolder(@NonNull View itemView) {
            super(itemView);
            learnSetImg = itemView.findViewById(R.id.learnSetImg);
            typeQuestionName = itemView.findViewById(R.id.typeQuestionName);
            questTypeDesc = itemView.findViewById(R.id.questTypeDesc);
            progressBarLearnedQuest = itemView.findViewById(R.id.progressBarLearnedQuest);
            numOfLearnedQuest = itemView.findViewById(R.id.numOfLearnedQuest);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if(listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(questionTypeList.get(position));
                    }
                }
            });
        }
    }
    public interface OnItemClickListener{
        void onItemClick(QuestionType questionType);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
