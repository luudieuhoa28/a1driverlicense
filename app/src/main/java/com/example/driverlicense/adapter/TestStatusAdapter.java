package com.example.driverlicense.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.driverlicense.R;
import com.example.driverlicense.entity.Question;
import com.example.driverlicense.entity.TestQuestion;

import java.util.ArrayList;
import java.util.List;

//adapter for test status in sliding up panel
// test status show is this question is selected or not | correct or not
public class TestStatusAdapter extends RecyclerView.Adapter<TestStatusAdapter.TestStatusViewHoler> {
    private List<Question> listQuestion = new ArrayList<>();
    private List<TestQuestion> testQuestionList = new ArrayList<>();
    private OnItemClickListener listener;
    int typeAction;

    public TestStatusAdapter(List<TestQuestion> testQuestionList, int typeAction) {
        this.testQuestionList = testQuestionList;
        this.typeAction = typeAction;
    }

    public TestStatusAdapter(int typeAction, List<Question> listQuestion) {
        this.listQuestion = listQuestion;
        this.typeAction = typeAction;
    }

    @NonNull
    @Override
    public TestStatusViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.status_question_item, parent, false);
        return new TestStatusAdapter.TestStatusViewHoler(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TestStatusViewHoler holder, int position) {
        if (typeAction == 2) {
            TestQuestion testQuestion = testQuestionList.get(position);
            holder.statusQuestItemTextView.setText(position + 1 + "");
            if (testQuestion.getAnswer() != 0) {
                holder.statusQuestItemTextView.setTextColor(Color.GREEN);
            } else {
                holder.statusQuestItemTextView.setTextColor(Color.BLACK);
            }
        } else {
            Question question = listQuestion.get(position);
            holder.statusQuestItemTextView.setText(position + 1 + "");
            if (question.getMark() != 0) {
                holder.statusQuestItemTextView.setTextColor(Color.GREEN);
            } else {
                holder.statusQuestItemTextView.setTextColor(Color.BLACK);
            }
        }

    }

    @Override
    public int getItemCount() {
        if (typeAction == 2) {
            return testQuestionList.size();
        }
        return listQuestion.size();
    }

    public class TestStatusViewHoler extends RecyclerView.ViewHolder {
        TextView statusQuestItemTextView;

        public TestStatusViewHoler(@NonNull View itemView) {
            super(itemView);
            statusQuestItemTextView = itemView.findViewById(R.id.statusQuestItemTextView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
