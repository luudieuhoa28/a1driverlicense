package com.example.driverlicense.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.driverlicense.R;
import com.example.driverlicense.entity.Test;

import java.util.ArrayList;
import java.util.List;

//adapter for test item list
//this test item list will be shown after click "thi" button
//this list is for user to choose the test set to do
public class TestAdapter extends RecyclerView.Adapter<TestAdapter.TestHolder> {
    List<Test> testList = new ArrayList<>();
    private OnItemClickListener listener;

    public TestAdapter(List<Test> testList) {
        this.testList = testList;
    }

    public List<Test> getTestList() {
        return testList;
    }

    public void setTestList(List<Test> testList) {
        this.testList = testList;
    }

    @NonNull
    @Override
    public TestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_item, parent, false);
        return new TestHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TestHolder holder, int position) {
        Test currentTest = testList.get(position);
        if (currentTest.isFinish()) {
            if (currentTest.isFailed()) {
                holder.trangThaiThiTextView.setText("Trượt");
                if (currentTest.getTotalSuccess() == 0) {
                    holder.soCauHienTaiTextView.setText("Sai câu điểm liệt");
                } else {
                    holder.soCauHienTaiTextView.setText(currentTest.getTotalSuccess() + "/25");
                }
                holder.thoiGianTextView.setText(getTimerFormat(currentTest.getCurrentTime()) + "");
            }
            if (!currentTest.isFailed()){
                holder.trangThaiThiTextView.setText("Đậu");
                holder.soCauHienTaiTextView.setText(currentTest.getTotalSuccess() + "/25");
                holder.thoiGianTextView.setText(getTimerFormat(currentTest.getCurrentTime()) + "");
            }
        } else {
            if (currentTest.getCurrentTime() == 30000) {
                holder.trangThaiThiTextView.setText("Làm bài");
                holder.soCauHienTaiTextView.setText("25 câu/19 phút");

            } else {
                holder.trangThaiThiTextView.setText("Tiếp tục");
                holder.thoiGianTextView.setText(getTimerFormat(currentTest.getCurrentTime()) + "");
            }
        }
        holder.tenBoDeTextView.setText("Bộ đề " + currentTest.getNameTest());

    }

    @Override
    public int getItemCount() {
        return testList.size();
    }

    class TestHolder extends RecyclerView.ViewHolder {
        private TextView tenBoDeTextView;
        private TextView trangThaiThiTextView;
        private TextView soCauHienTaiTextView;
        private TextView thoiGianTextView;

        public TestHolder(@NonNull View itemView) {
            super(itemView);
            tenBoDeTextView = itemView.findViewById(R.id.tenBoDeTextView);
            trangThaiThiTextView = itemView.findViewById(R.id.trangThaiThiTextView);
            soCauHienTaiTextView = itemView.findViewById(R.id.soCauHienTaiTextView);
            thoiGianTextView = itemView.findViewById(R.id.thoiGianTextView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if(listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(testList.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClick(Test test);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public String getTimerFormat(int timeLeftMiniSecond) {
        int minu = (int) (timeLeftMiniSecond / 60000);
        int second = (int) (timeLeftMiniSecond % 60000 / 1000);

        String timer = "" + minu;
        timer += ":";
        if (second < 10) {
            timer += "0";
        }
        timer += second;
       return timer;
    }
}
