package com.example.driverlicense.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.driverlicense.entity.Sign;
import com.example.driverlicense.entity.SignCategory;
import com.example.driverlicense.fragment.SignFragment;

import java.util.ArrayList;
import java.util.List;

public class SignPagerAdapter extends FragmentPagerAdapter {
    List<SignCategory> signCategoryList = new ArrayList<>();
    public SignPagerAdapter(@NonNull FragmentManager fm, List<SignCategory> signCategoryList) {
        super(fm);
        this.signCategoryList = signCategoryList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return SignFragment.newInstance(signCategoryList.get(position));
    }

    @Override
    public int getCount() {
        return signCategoryList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return signCategoryList.get(position).getSignCateName();
    }
}
